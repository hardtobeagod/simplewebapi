﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimpleWebApi.Models;

namespace SimpleWebApi.App_code
{
    public static class Global
    {
        /// <summary>
        /// Global variable storing important stuff.
        /// </summary>
        static List<Person> _importantData = new List<Person>()
        {
            new Person {id = 1, name = "Ivan", surname = "Ivanovich"}
        };

        /// <summary>
        /// Get or set the static important data.
        /// </summary>
        public static List<Person> ImportantData
        {
            get
            {
                return _importantData;
            }
            set
            {
                _importantData = value;
            }
        }
    }
}
