﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleWebApi.Models
{
    /// <summary>
    /// Model class.
    /// Describes Person object.
    /// </summary>
    public class Person
    {
        ///<value>Person id</value>
        public int id { get; set; }
        ///<value>Person name</value>
        public string name { get; set; }
        ///<value>Person surname</value>
        public string surname { get; set; }
    }
}
