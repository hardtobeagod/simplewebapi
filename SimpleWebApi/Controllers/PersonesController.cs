﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleWebApi.Models;
using SimpleWebApi.App_code;




namespace SimpleWebApi.Controllers
{
    /// <summary>
    /// Controller class.
    /// Contains all api metods for Person model class
    /// </summary>
    [Route("api/")]
    [ApiController]
    public class PersonesController : Controller
    {        
        // GET: api/get
        [HttpGet("get")]
        /// <value>Gets all Person objects from the list of Persons</value>
        public ActionResult<IEnumerable<Person>> GetPersons()
        {
            return Ok(Global.ImportantData);
        }

        // GET api/get/5
        [HttpGet("get/{id}")]
        /// <value>Gets Person object from the list of Persons by its id </value>
        public ActionResult<Person> GetPerson(int id)
        {
            var person = Global.ImportantData.FirstOrDefault((p) => p.id == id);
            if(person == null)
            {
                return NotFound();
            }
            return Ok(person);
        }

        // POST api/post
        [HttpPost("post")]
        /// <value>Creates a new Person object in list of Persons</value>
        /// <remarks>Takes json Person object as input. Checs that Person name must be uniq</remarks>
        public ActionResult<Person> CreatePerson(Person person)
        {

            // обработка частных случаев валидации
            if (person.name == null)
            {
                ModelState.AddModelError("name", "Имя должно быть указано");
            }

            if (person.surname == null)
            {
                ModelState.AddModelError("surname", "Фамилия должна указана");
            }

            if (person.id == 0)
            {
                ModelState.AddModelError("id", "Идентификатор должен быть указан");
            }

            if (Global.ImportantData.Exists((p) => p.name == person.name))
            {
                ModelState.AddModelError("name", "Имя должно быть уникальным");
            }

            if (Global.ImportantData.Exists((p) => p.id == person.id))
            {
                ModelState.AddModelError("name", "Идентификатор должен быть уникальным");
            }

            // если есть ошибки - возвращаем ошибку 400
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Global.ImportantData.Add(person);
            return Ok(person);
        }

        // PUT api/patch/5
        [HttpPut("patch/{id}")]
        /// <value>Change a Person object in list of Persons</value>
        /// <remarks>Takes json Person object as input. Also check that url id equals object id.</remarks>
        public ActionResult<Person> ChangePerson(int id, Person person)
        {
            if(id != person.id)
            {
                return BadRequest();
            }

            var modifiedperson = Global.ImportantData.FirstOrDefault((p) => p.id == id);
            if (modifiedperson == null)
            {
                return NotFound();
            }

            modifiedperson.name = person.name;
            modifiedperson.surname = person.surname;

            return Ok(person);
        }

        // DELETE api/<controller>/5
        // TODO: For realisation. 
        /// <remarks>Delete person object from list</remarks>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
